/*
 * BAC1 - Travaux pratiques de mathématiques appliquées
 *
 * Laboratoire 3 - Les ensembles
 *               - Les collections Java
 *
 */
package labo3;

import java.util.Arrays;

/**
 * La classe 'IntegerSet' modélise un ensemble de nombres entiers.<br>
 * <br>
 * Les éléments de l'ensemble sont stockés dans un tableau d'entiers.<br>
 * Les éléments du tableau sont triés.<br>
 * Par définition d'un ensemble, il n'y a pas de doublons.<br>
 *
 * @author François Schumacker
 */
public final class IntegerSet {

    private int[] data;

    /**
     * Instancie un ensemble vide.
     */
    public IntegerSet() {
        this.data = null;
    }

    /**
     * Instancie un ensemble contenant les valeurs du tableau fourni.
     *
     * @param data Tableau de nombres entiers
     */
    public IntegerSet(int data[]) {
        /*
         * NOTE : il n'est pas possible de copier simplement le tableau,
         * car il faut garantir l'unicité et l'ordre des éléments.
         */
        if (data == null) {
            this.data = null; // IntegerSet vide
        } else {
            // Méthode naïve : on ajoute les éléments 1 par 1
            for (int elem : data) {
                this.add(elem);
            }
        }
    }

    /**
     * Instancie un ensemble qui est la copie de l'ensemble fourni.
     *
     * @param set ensemble dont on veut créer un copie
     */
    public IntegerSet(IntegerSet set) {
        if (set == null || set.data == null) {
            this.data = null;
        } else {
            /*
             * Ici, on peut copier le tableau directement,
             * car il provient d'un IntegerSet. Il est donc trié
             * et ne contient pas de doublons.
             */
            this.data = Arrays.copyOf(set.data, set.data.length);
        }
    }

    /**
     * Accesseur présent uniquement pour les besoins du plan de test
     *
     * @return le tableau contenant les éléments de cet ensemble
     */
    public int[] getData() {
        return this.data;
    }

    /**
     * Ajoute la valeur spécifiée à cet ensemble si elle ne s'y trouve pas déjà.
     *
     * @param value valeur à ajouter à l'ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean add(int value) {
        boolean changed = false;
        if (this.data == null) {
            // Si l'ensemble est vide on crée un tableau contenant l'unique élément
            this.data = new int[1];
            this.data[0] = value;
            changed = true;
        } else {
            // Sinon, on cherche si l'élément est déjà présent
            int index = Arrays.binarySearch(this.data, value);

            // Si l'élément est déjà présent (index >= 0), on ne doit rien faire.
            // Sinon, il faut l'ajouter à l'ensemble en respectant le tri
            if (index < 0) {
                // Calcul du point d'insertion
                index = -(index + 1);
                // On crée un tableau avec 1 case de plus
                int newData[] = new int[this.data.length + 1];
                // On copie les éléments [0, index[ de l'ancien tableau vers le nouveau
                System.arraycopy(this.data, 0, newData, 0, index);
                // On insère le nouvel élément à la bonne place
                newData[index] = value;
                // On copie les éléments [index, length[ de l'ancien tableau vers le nouveau
                System.arraycopy(this.data, index, newData, index + 1, this.data.length - index);
                // On remplace le tableau de données par le nouveau
                this.data = newData;
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Efface toutes les valeurs de cet ensemble.
     */
    public void clear() {
        this.data=null;
        // à compléter ...
    }

    /**
""     * Renvoie true si cet ensemble contient la valeur fournie.
     *
     * @param value valeur dont on veut tester la présence dans cet ensemble
     * @return true si cet ensemble contient la valeur fournie
     */
    public boolean contains(int value) {
        return !this.isEmpty() && Arrays.binarySearch(this.getData(), value) >= 0;
    }

    /**
     * Compare cet ensemble et l'ensemble fourni pour déterminer s'ils
     * contiennent les même valeurs.
     *
     * @param set ensemble dont on veut tester l'égalité avec cet ensemble
     * @return true si les deux ensembles contiennent les mêmes valeurs
     */
    public boolean equals(IntegerSet set) {
        return  set !=null && Arrays.equals(this.getData(), set.getData()); 
    }

    /**
     * Intersection entre cet ensemble et l'ensemble fourni.
     *
     * @param other ensemble dont on veut calculer l'intersection avec cet
     * ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean intersection(IntegerSet other) {
        boolean changed = false;
        if(this.isEmpty()) return changed;
        for(int value : this.getData()){
            if(!other.contains(value)&& this.remove(value)) changed = true;
        }
        return changed;
    }

    /**
     * Renvoie true si cet ensemble est vide (ne contient aucun élément).
     *
     * @return true si l'ensemble est vide
     */
    public boolean isEmpty() {
        return this.data == null;
    }

    /**
     * Retire de cet ensemble toutes les valeurs qui sont contenues dans
     * l'ensemble fourni.
     *
     * @param set ensemble contenant les valeurs à retirer
     * @return true si l'ensemble a été modifié
     */
    public boolean minus(IntegerSet set) {
        boolean changed = false;
        if(set.isEmpty()) return changed;
        for(int value : set.getData()){
            if(this.contains(value)&& this.remove(value)) changed = true;
        }
        return changed;
    }

    /**
     * Retire la valeur fournie de cet ensemble si elle est présente.
     *
     * @param value valeur que l'on souhaite retirer de cet ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean remove(int value) {
        boolean changed = true;
        if(this.isEmpty())return false;
        int i = Arrays.binarySearch(this.getData(), value);
        if (i >= 0) {
            if (this.size() == 1) {
                this.data = null;
                return changed;
            }
            int[] newData=new int[this.size()-1];
            System.arraycopy(this.getData(), 0, newData, 0, i);
            System.arraycopy(this.getData(), i+1, newData, i, (this.size()-1)-i);
            this.data=newData;
            return changed;
        }
        else return false;
        
    }

    /**
     * Renvoie le nombre de valeurs présentes dans cet ensemble (cardinalité).
     *
     * @return cardinalité de l'ensemble
     */
    public int size() {
        if(this.isEmpty())return 0;
        return this.getData().length;
    }

    /**
     * Crée une chaîne de caractères qui décrit le contenu de cet ensemble.
     *
     * @return chaîne décrivant le contenu de cet ensemble
     */
    @Override
    public String toString() {
        String s = "";
        if (this.data != null) {
            for (int elem : this.data) {
                if (s.length() == 0) {
                    s += "[" + elem;
                } else {
                    s += ", " + elem;
                }
            }
            s += "]";
        } else {
            s = "[]";
        }
        return s;
    }

    /**
     * Union entre cet ensemble et l'ensemble fourni.
     *
     * @param other ensemble dont on veut calculer l'union avec cet ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean union(IntegerSet other) {
        boolean changed = false;
        if(other.isEmpty())return changed;
        for(int value: other.getData()){
            if(!this.contains(value)&& this.add(value)) changed = true;
        }
        // à compléter ...
        return changed;
    }

    /**
     * Compte le nombre de valeurs présentent simultanément dans cet ensemble et
     * dans l'ensemble fourni.
     *
     * @param other ensemble de valeurs
     * @return nombre de valeurs communes aux deux ensembles
     */
    public int commonCount(IntegerSet other) {
        int count = 0;
        if (this.data != null && other != null && other.data != null) {
            int i1 = 0; // position dans le 1er tableau
            int i2 = 0; // position dans le 2eme tableau
            
            // Tant qu'on n'a pas atteint la fin d'un des 2 tableaux
            while (i1 < this.data.length && i2 < other.data.length) {
                // Si l'élément courant du premier tableau est plus petit que
                // l'élément courant du second, on avance d'une case dans le
                // premier tableau.
                if (this.data[i1] < other.data[i2]) {
                    i1++;
                }
                // Si l'élément courant du second tableau est plus petit que
                // l'élément courant du premier, on avance d'une case dans le
                // second tableau.
                else if (other.data[i2] < this.data[i1]) {
                    i2++;
                }
                // Sinon, l'élément courant du premier tableau est égal à
                // l'élément courant du second. On incrémente le compteur
                // et on avance d'une case dans les deux tableaux.
                else {
                    count++;
                    i1++;
                    i2++;
                }
            }
        }
        return count;
    }

    /**
     * Intersection entre cet ensemble et l'ensemble fourni. (si other ne contiens pas la val on l efface de this
     *
     * @param other ensemble dont on veut calculer l'intersection avec cet
     * ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean intersectionOpt(IntegerSet other) {
        boolean changed = false;
        if (this.data != null && other != null && other.data != null) {
            int i1 = 0; // position dans le 1er tableau
            int i2 = 0; // position dans le 2eme tableau
            int nsValues =this.commonCount(other);
            int newData[] = new int[nsValues];
            int count =0;
            // Tant qu'on n'a pas atteint la fin d'un des 2 tableaux
            while (nsValues>0&&i1 < this.data.length && i2 < other.data.length) {
                // Si l'élément courant du premier tableau est plus petit que
                // l'élément courant du second, on avance d'une case dans le
                // premier tableau.
                if (this.data[i1] < other.data[i2]) {
                    i1++;
                }
                // Si l'élément courant du second tableau est plus petit que
                // l'élément courant du premier, on avance d'une case dans le
                // second tableau.
                else if (other.data[i2] < this.data[i1]) {
                    i2++;
                }
                // Sinon, l'élément courant du premier tableau est égal à
                // l'élément courant du second. On incrémente le compteur
                // et on avance d'une case dans les deux tableaux.
                else {
                    newData[count]=data[i1];
                    nsValues --;
                    i1++;
                    i2++;
                }
            }
            this.data=newData;
            changed=true;
        }
        return changed;

            
        
    }

    /**
     * Soustraction entre cet ensemble et l'ensemble fourni.
     *
     * @param other ensemble que l'on souhaite soustraire à cet ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean minusOpt(IntegerSet other) {
        boolean changed = false;
        // à compléter ...
        return changed;
    }

    /**
     * Union entre cet ensemble et l'ensemble fourni.
     *
     * @param other ensemble dont on veut calculer l'union avec cet ensemble
     * @return true si l'ensemble a été modifié
     */
    public boolean unionOpt(IntegerSet other) {
        boolean changed = false;
        // à compléter ...
        return changed;
    }
}
