/*
 * BAC1 - Travaux pratiques de mathématiques appliquées
 *
 * Laboratoire 3 - Les ensembles
 *               - Les collections Java
 *
 */
package labo3;

import java.util.TreeSet;

/**
 * Classe de démarrage (main) du labo 3.
 * 
 * @author François Schumacker
 */
public class Labo3 {

    private static final int BORNE_MAX = 50;
    private static final IntegerSet integerSetVide = new IntegerSet();
    private static final TreeSet<Integer> treeSetVide = new TreeSet();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Exemple d'utilisation de la classe IntergerSet
        exempleIntegerSet();

        // Exercice 2 - Multiples de 3 et 5 - IntergerSet
        multiplesDe3Et5IntegerSet();
        
        /*
         * Exercice 3 - Les collections Java - TreeSet<Integer>
         */
        exempleTreeSet();
        multiplesDe3Et5TreeSet();

        /*
         * Exercice 4 - Les experts à HELMo...
         */
        expertsHELMo();
    }

    /*
     * Exemple d'utilisation de la classe IntergerSet
     */
    public static void exempleIntegerSet() {
        System.out.println("");
        System.out.println("Exemple - Version 1 - IntegerSet");
        System.out.println("--------------------------------");
        System.out.println("");

        // Initialisation des ensembles de départ
        // Première méthode : créer un ensemble vide et y ajouter les éléments.
        IntegerSet s1 = new IntegerSet();
        s1.add(1); s1.add(2); s1.add(3);
        afficherIntegerSet("s1 = ", s1);
        
        // Deuxième méthode : partir d'un tableau d'entiers
        IntegerSet s2 = new IntegerSet(new int[]{2, 3, 5, 7});
        afficherIntegerSet("s2 = ", s2);
        IntegerSet s3 = new IntegerSet(new int[]{0, 2, 5, 7, 9});
        afficherIntegerSet("s3 = ", s3);
        IntegerSet s4 = new IntegerSet(new int[]{2, 5});
        afficherIntegerSet("s4 = ", s4);
        System.out.println("");

        // Initialisation de l'ensemble de résultat s
        // -> initialement il est égal à s1
        IntegerSet s = new IntegerSet(s1);

        // UNION avec s2
        s.union(s2);
        afficherIntegerSet("s1 union s2 = ", s);

        // INTERSECTION avec s3
        s.intersection(s3);
        afficherIntegerSet("(s1 union s2) inter s3 = ", s);

        // SOUSTRACTION de s4
        s.minus(s4);
        afficherIntegerSet("((s1 union s2) inter s3) minus s4) = ", s);

        System.out.println("");
    }

    /*
     * Exercice 2 - Multiples de 3 et 5 - IntergerSet
     */
    public static void multiplesDe3Et5IntegerSet() {
        System.out.println("Multiples de 3 et 5 - Version 1 - IntegerSet");
        System.out.println("--------------------------------------------");
        System.out.println("");

        // Ensemble des multiples de 3 dans [0, BORNE_MAX]
        IntegerSet mult3 = new IntegerSet();
        // à compléter ...
        afficherIntegerSet("Mult. de 3 : ", mult3);

        // Ensemble des multiples de 5 dans [0, BORNE_MAX]
        IntegerSet mult5 = new IntegerSet();
        // à compléter ...
        afficherIntegerSet("Mult. de 5 : ", mult5);

        // Multiples de 3 ou de 5
        IntegerSet mult3ou5 = integerSetVide; // <- à adapter
        // à compléter ...
        afficherIntegerSet("Mult. de 3 ou de 5 : ", mult3ou5);

        // Multiples de 3 et 5
        IntegerSet mult3et5 = integerSetVide; // <- à adapter
        // à compléter ...
        afficherIntegerSet("Mult. de 3 et de 5 : ", mult3et5);

        // Multiples de 3 non multiples de 5
        IntegerSet mult3pas5 = integerSetVide; // <- à adapter
        // à compléter ...
        afficherIntegerSet("Mult. de 3 et pas de 5 : ", mult3pas5);

        // Multiples de 5 non multiples de 3
        IntegerSet mult5pas3 = integerSetVide; // <- à adapter
        // à compléter ...
        afficherIntegerSet("Mult. de 5 et pas de 3 : ", mult5pas3);

        // Multiples de 5 ou de 3, mais pas les deux en même temps
        IntegerSet mult3xor5 = integerSetVide; // <- à adapter
        // à compléter ...
        afficherIntegerSet("Mult. de 5 ou de 3, mais pas les 2 : ", mult3xor5);

        System.out.println("");
    }

    /*
     * Exercice 3 - Utilisation d'une collection TreeSet<Integer>
     *
     * Exercice 3.1 - Adaptation de la méthode 'exempleIntegerSet()'
     */
    public static void exempleTreeSet() {
        System.out.println("Exemple - Version 2 - TreeSet<Integer>");
        System.out.println("--------------------------------------");
        System.out.println("");

        // Initialisation des ensembles de départ
        // Première méthode : créer un ensemble vide et y ajouter les éléments.
        // à compléter ...
        afficherTreeSet("s1 = ", treeSetVide); // <- à adapter

        // Deuxième méthode : partir d'un tableau d'entiers
        // à compléter ...
        afficherTreeSet("s2 = ", treeSetVide); // <- à adapter
        
        // à compléter ...
        afficherTreeSet("s3 = ", treeSetVide); // <- à adapter
        
        // à compléter ...
        afficherTreeSet("s4 = ", treeSetVide); // <- à adapter
        System.out.println("");

        // Initialisation de l'ensemble de résultat s
        // -> initialement il est égal à s1
        // à compléter ...


        // UNION avec s2
        // à compléter ...
        afficherTreeSet("s1 union s2 = ", treeSetVide); // <- à adapter

        // INTERSECTION avec s3
        // à compléter ...
        afficherTreeSet("(s1 union s2) inter s3 = ", treeSetVide); // <- à adapter

        // SOUSTRACTION de s4
        // à compléter ...
        afficherTreeSet("((s1 union s2) inter s3) minus s4) = ", treeSetVide); // <- à adapter

        System.out.println("");
    }

    /*
     * Exercice 3.2 - Multiples de 3 et 5 - Version 2 - TreeSet<Integer>
     */
     public static void multiplesDe3Et5TreeSet() {
        System.out.println("Multiples de 3 et 5 - Version 2 - TreeSet<Integer>");
        System.out.println("--------------------------------------------------");
        System.out.println("");

        // Ensemble des multiples de 3 dans [0, BORNE_MAX]
        // à compléter ...
        afficherTreeSet("Mult. de 3 : ", treeSetVide); // <- à adapter

        // Ensemble des multiples de 5 dans [0, BORNE_MAX]
        // à compléter ...
        afficherTreeSet("Mult. de 5 : ", treeSetVide); // <- à adapter

        // Multiples de 3 ou de 5
        // à compléter ...
        afficherTreeSet("Mult. de 3 ou de 5 : ", treeSetVide); // <- à adapter

        // Multiples de 3 et 5
        // à compléter ...
        afficherTreeSet("Mult. de 3 et de 5 : ", treeSetVide); // <- à adapter

        // Multiples de 3 non multiples de 5
        // à compléter ...
        afficherTreeSet("Mult. de 3 et pas de 5 : ", treeSetVide); // <- à adapter

        // Multiples de 5 non multiples de 3
        // à compléter ...
        afficherTreeSet("Mult. de 5 et pas de 3 : ", treeSetVide); // <- à adapter

        // Multiples de 5 ou de 3, mais pas les deux en même temps
        // à compléter ...
        afficherTreeSet("Mult. de 5 ou de 3, mais pas les 2 : ", treeSetVide); // <- à adapter

        System.out.println("");
    }

    /*
     * Exercice 4 - Les Experts à HELMo
     */
    public static void expertsHELMo() {
        System.out.println("Les Experts à HELMo");
        System.out.println("-------------------");

        rechercheSuspecte();
        rechercheSuspect();
    }

    /*
     * Exercice 4.1 : recherche de la suspecte
     */
    public static void rechercheSuspecte() {
        System.out.println("");
        System.out.println(">>> A la recherche de la suspecte...");
        System.out.println("");

        // Point de départ : c'est une femme ! :-)
        TreeSet<Suspect> femmes = Suspect.searchSuspectDatabase("sexe", "F");
        Suspect.printSuspectsNames(femmes);

        // à compléter ...

        if (femmes.size() == 1) {
            // Affichage de la fiche détaillée
            System.out.println("");
            System.out.println("Fiche descriptive de la suspecte");
            System.out.println("--------------------------------");
            System.out.println("");
            for (Suspect s : femmes) {
                Suspect.printSuspectDetails(s);
            }
        } else {
            // L'enquête n'a pas abouti !
            System.out.println("");
            System.out.println("La suspecte n'a pas été identifiée !");
        }
    }

    /*
     * Recherche du suspect
     */
    public static void rechercheSuspect() {
        System.out.println("");
        System.out.println(">>> A la recherche du suspect...");
        System.out.println("");

        // Point de départ : c'est un homme ! :-)
        TreeSet<Suspect> hommes = Suspect.searchSuspectDatabase("sexe", "M");
        Suspect.printSuspectsNames(hommes); // Affichage des noms des suspects

        // à compléter ...

        if (hommes.size() == 1) {
            // Affichage de la fiche détaillée
            System.out.println("");
            System.out.println("Fiche descriptive du suspect");
            System.out.println("----------------------------");
            System.out.println("");
            for (Suspect s : hommes) {
                Suspect.printSuspectDetails(s);
            }
        } else {
            // L'enquête n'a pas abouti !
            System.out.println("");
            System.out.println("Le suspect n'a pas été identifié !");
        }
    }

    /**
     * Affiche la description fournie suivie du contenu et de la cardinalité de
     * l'IntegerSet fourni
     *
     * @param description la description à afficher
     * @param s l'ensemble à afficher
     */
    public static void afficherIntegerSet(String description, IntegerSet s) {
        System.out.print(description == null ? "null" : description);
        System.out.println(s == null ? "null" : s.toString() + " #" + s.size());
    }

    /**
     * Affiche la description fournie suivie du contenu et de la cardinalité du
     * TreeSet fourni
     *
     * @param description la description à afficher
     * @param s l'ensemble à afficher
     */
    public static void afficherTreeSet(String description, TreeSet<Integer> s) {
        System.out.print(description == null ? "null" : description);
        System.out.println(s == null ? "null" : s.toString() + " #" + s.size());
    }
}
