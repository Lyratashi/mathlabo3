/*
 * BAC1 - Travaux pratiques de mathématiques appliquées
 *
 * Laboratoire 3 - Les ensembles
 *               - Les collections Java
 *
 */
package labo3;

import java.util.Arrays;

/**
 * Plan de test du labo 3 - Version 1.
 * 
 * @author François Schumacker
 */

public class Test {

    /**
     * Méthode principale du plan de test
     *
     * @param args sans objet
     */
    public static void main(String[] args) {
        System.out.println("--- Constructeurs ---\n");
        testIntegerSetArray();
        testIntegerSetCopy();
        System.out.println();

        System.out.println("--- Opérations de test ---\n");
        testContains();
        testEquals();
        testIsEmpty();
        System.out.println();
 
        System.out.println("--- Opérations de base (add, clear, remove, size) ---\n");
        testAdd();
        testClear();
        testRemove();
        testSize();
        System.out.println();

        System.out.println("--- Opérations ensemblistes (naïves) ---\n");
        testIntersection();
        testMinus();
        testUnion();
        System.out.println();

        System.out.println("--- Opérations ensemblistes (optimisées) ---\n");
        testCommonCount();
        testIntersectionOpt();
        testMinusOpt();
        testUnionOpt();
        System.out.println();
    }

    /*
     * Tests des constructeurs
     */
    private static void testIntegerSetArray() {
        System.out.print("public IntegerSet(int data[]) -> ");
        IntegerSet s = new IntegerSet((int [])null);
        assert (s != null && s.getData() == null);
        
        s = new IntegerSet(new int []{-1,2,3});
        assert (s != null && Arrays.equals(s.getData(), new int []{-1,2,3}));
        
        s = new IntegerSet(new int []{3,2,-1});
        assert (s != null && Arrays.equals(s.getData(), new int []{-1,2,3}));
        
        s = new IntegerSet(new int []{3,2,3,3,2,1,1});
        assert (s != null && Arrays.equals(s.getData(), new int []{1,2,3}));     
        System.out.println("ok");
    }

    private static void testIntegerSetCopy() {
        System.out.print("public IntegerSet(IntegerSet set) -> ");
        IntegerSet s = new IntegerSet((IntegerSet)null);
        assert (s != null && s.getData() == null);
        
        s = new IntegerSet(new IntegerSet());
        assert (s != null && s.getData() == null);
        
        IntegerSet s1 = new IntegerSet(new int []{3,2,3,3,2,1,1});
        s = new IntegerSet(s1);
        assert (s != null && s.getData() != s1.getData());
        assert (Arrays.equals(s.getData(), s1.getData()));
        System.out.println("ok");
    }

    /*
     * Tests des opérations de test
     */
    private static void testContains() {
        System.out.print("public boolean contains(int value) -> ");
        IntegerSet s0 = new IntegerSet();
        assert (s0.contains(0) == false);
        
        IntegerSet s = new IntegerSet(new int []{1,2,3,4,5});
        assert (s.contains(1) == true);
        assert (s.contains(3) == true);
        assert (s.contains(5) == true);
        assert (s.contains(0) == false);
        assert (s.contains(10) == false);
        System.out.println("ok");
    }

    private static void testEquals() {
        System.out.print("public boolean equals(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s0bis = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3});
        IntegerSet s1bis = new IntegerSet(new int []{1,2,3});
        IntegerSet s2 = new IntegerSet(new int []{3});
        IntegerSet s3 = new IntegerSet(new int []{1,2,3,4});
        assert (!s0.equals(null));
        assert (!s1.equals(null));
        assert (s0.equals(s0bis));
        assert (!s0.equals(s1));
        assert (!s1.equals(s0));
        assert (s1.equals(s1));
        assert (s1.equals(s1bis));
        assert (!s1.equals(s2));
        assert (!s1.equals(s3));
        System.out.println("ok");
    }

    private static void testIsEmpty() {
        System.out.print("public boolean isEmpty() -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3});
        IntegerSet s2 = new IntegerSet(new int []{3});
        assert (s0.isEmpty());
        assert (!s1.isEmpty());
        assert (!s2.isEmpty());
        System.out.println("ok");
    }

    /*
     * Tests des opérations de base (add, clear, remove, size)
     */
    private static void testAdd() {
        System.out.print("public boolean add(int value) -> ");
        IntegerSet s = new IntegerSet();
        assert (s.add(5) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{5}));
        assert (s.add(1) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,5}));
        assert (s.add(10) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,5,10}));
        assert (s.add(1) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,5,10}));
        assert (s.add(5) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,5,10}));
        assert (s.add(10) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,5,10}));
        System.out.println("ok");
    }

    private static void testClear() {
        System.out.print("public void clear() -> ");
        IntegerSet s = new IntegerSet(new int []{1,2,3});
        s.clear();
        assert (s.getData() == null);
        System.out.println("ok");
    }

    private static void testRemove() {
        System.out.print("public boolean remove(int value) -> ");
        IntegerSet s = new IntegerSet(new int []{1,3,5,7});
        assert (s.remove(0) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,3,5,7}));
        assert (s.remove(4) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,3,5,7}));
        assert (s.remove(11) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,3,5,7}));
        assert (s.remove(5) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{1,3,7}));
        assert (s.remove(1) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{3,7}));
        assert (s.remove(7) == true);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{3}));
        assert (s.remove(0) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{3}));
        assert (s.remove(5) == false);
        assert (s.getData() != null);
        assert (Arrays.equals(s.getData(), new int []{3}));
        assert (s.remove(3) == true);
        assert (s.getData() == null);
        assert (s.remove(10) == false);
        assert (s.getData() == null);
        System.out.println("ok");
    }

    private static void testSize() {
        System.out.print("public int size() -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3});
        IntegerSet s2 = new IntegerSet(new int []{3});
        assert (s0.size() == 0);
        assert (s1.size() == 3);
        assert (s2.size() == 1);
        System.out.println("ok");
    }

    /*
     * Tests des opérations ensemblistes (versions naïves)
     */
    private static void testIntersection() {
        System.out.print("public boolean intersection(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{0,1,3,5,6});
        IntegerSet s3 = new IntegerSet(new int []{0,6});
        
        assert (s0.intersection(s0) == false);
        assert (s0.getData() == null);
        
        assert (s0.intersection(s1) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.intersection(s1) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.intersection(s2) == true);
        assert Arrays.equals(s1.getData(), new int []{1,3,5});
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        
        assert (s1.intersection(s3) == true);
        assert (s1.getData() == null);
        assert Arrays.equals(s3.getData(), new int []{0,6});
        
        assert (s3.intersection(s0) == true);
        assert (s3.getData() == null);
        assert (s0.getData() == null);
        System.out.println("ok");
    }

    private static void testMinus() {
        System.out.print("public boolean minus(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{0,1,3,5,6});
        IntegerSet s3 = new IntegerSet(new int []{2,4});
        
        assert (s0.minus(s0) == false);
        assert (s0.getData() == null);
        
        assert (s0.minus(s1) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.minus(s0) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.minus(s2) == true);
        assert Arrays.equals(s1.getData(), new int []{2,4});
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        
        assert (s1.minus(s3) == true);
        assert (s1.getData() == null);
        assert Arrays.equals(s3.getData(), new int []{2,4});
        
        assert (s2.minus(s3) == false);
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        assert Arrays.equals(s3.getData(), new int []{2,4});
        System.out.println("ok");
    }

    private static void testUnion() {
        System.out.print("public boolean union(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{2,4});
        IntegerSet s3 = new IntegerSet(new int []{0,1,3,5,6});
        
        assert (s0.union(s0) == false);
        assert (s0.getData() == null);
        
        assert (s1.union(s0) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s0.union(s1) == true);
        assert Arrays.equals(s0.getData(), new int []{1,2,3,4,5});
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.union(s1) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.union(s2) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        assert Arrays.equals(s2.getData(), new int []{2,4});
        
        assert (s1.union(s3) == true);
        assert Arrays.equals(s1.getData(), new int []{0,1,2,3,4,5,6});
        assert Arrays.equals(s3.getData(), new int []{0,1,3,5,6});
        System.out.println("ok");
    }

    /*
     * Tests des opérations ensemblistes (versions optimisées)
     */
    private static void testCommonCount() {
        System.out.print("public int commonCount(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s0bis = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,3,5});
        IntegerSet s2 = new IntegerSet(new int []{3});
        IntegerSet s3 = new IntegerSet(new int []{2,3,4,5});
        IntegerSet s4 = new IntegerSet(new int []{2,4});
        IntegerSet s5 = new IntegerSet(new int []{0,2,4,5,7});
        assert (s0.commonCount(s0bis) == 0);
        assert (s0.commonCount(null) == 0);
        assert (s0.commonCount(s1) == 0);
        assert (s1.commonCount(s0) == 0);
        assert (s1.commonCount(null) == 0);
        assert (s1.commonCount(s1) == 3);
        assert (s1.commonCount(s2) == 1);
        assert (s2.commonCount(s1) == 1);
        assert (s1.commonCount(s3) == 2);
        assert (s3.commonCount(s1) == 2);
        assert (s1.commonCount(s4) == 0);
        assert (s4.commonCount(s1) == 0);
        assert (s1.commonCount(s5) == 1);
        assert (s5.commonCount(s1) == 1);
        System.out.println("ok");
    }

    private static void testIntersectionOpt() {
        System.out.print("public boolean intersectionOpt(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{0,1,3,5,6});
        IntegerSet s3 = new IntegerSet(new int []{0,6});
        
        assert (s0.intersectionOpt(s0) == false);
        assert (s0.getData() == null);
        
        assert (s0.intersectionOpt(s1) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.intersectionOpt(s1) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.intersectionOpt(s2) == true);
        assert Arrays.equals(s1.getData(), new int []{1,3,5});
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        
        assert (s1.intersectionOpt(s3) == true);
        assert (s1.getData() == null);
        assert Arrays.equals(s3.getData(), new int []{0,6});
        
        assert (s3.intersectionOpt(s0) == true);
        assert (s3.getData() == null);
        assert (s0.getData() == null);
        System.out.println("ok");
    }

    private static void testMinusOpt() {
        System.out.print("public boolean minusOpt(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{0,1,3,5,6});
        IntegerSet s3 = new IntegerSet(new int []{2,4});
        
        assert (s0.minusOpt(s0) == false);
        assert (s0.getData() == null);
        
        assert (s0.minusOpt(s1) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.minusOpt(s0) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.minusOpt(s2) == true);
        assert Arrays.equals(s1.getData(), new int []{2,4});
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        
        assert (s1.minusOpt(s3) == true);
        assert (s1.getData() == null);
        assert Arrays.equals(s3.getData(), new int []{2,4});
        
        assert (s2.minusOpt(s3) == false);
        assert Arrays.equals(s2.getData(), new int []{0,1,3,5,6});
        assert Arrays.equals(s3.getData(), new int []{2,4});
        System.out.println("ok");
    }

    private static void testUnionOpt() {
        System.out.print("public boolean unionOpt(IntegerSet set) -> ");
        IntegerSet s0 = new IntegerSet();
        IntegerSet s1 = new IntegerSet(new int []{1,2,3,4,5});
        IntegerSet s2 = new IntegerSet(new int []{2,4});
        IntegerSet s3 = new IntegerSet(new int []{0,1,3,5,6});
        
        assert (s0.unionOpt(s0) == false);
        assert (s0.getData() == null);
        
        assert (s1.unionOpt(s0) == false);
        assert s0.getData() == null;
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s0.unionOpt(s1) == true);
        assert Arrays.equals(s0.getData(), new int []{1,2,3,4,5});
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.unionOpt(s1) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        
        assert (s1.unionOpt(s2) == false);
        assert Arrays.equals(s1.getData(), new int []{1,2,3,4,5});
        assert Arrays.equals(s2.getData(), new int []{2,4});
        
        assert (s1.unionOpt(s3) == true);
        assert Arrays.equals(s1.getData(), new int []{0,1,2,3,4,5,6});
        assert Arrays.equals(s3.getData(), new int []{0,1,3,5,6});
        System.out.println("ok");
    }
}
