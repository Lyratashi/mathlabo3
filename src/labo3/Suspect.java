/*
 * BAC1 - Travaux pratiques de mathématiques appliquées
 *
 * Laboratoire 3 - Les ensembles
 *               - Les collections Java
 *
 */
package labo3;

import java.util.TreeSet;

/**
 * La classe 'Suspect' modélise une base de données de suspects.<br>
 * <br>
 * Elle est utilisable uniquement par le biais de 3 méthodes de classe:<br>
 *   - searchSuspectDatabase(String property, String value)<br>
 *   - printSuspectDetails(Suspect s)<br>
 *   - printSuspectsNames(TreeSet&lt;Suspect&gt; set)<br>
 * <br>
 * La fiche d'un suspect mentionne les informations suivantes :<br>
 *   - nom = <br>
 *   - sexe = (M, F)<br>
 *   - moustache = (oui, non)<br>
 *   - barbe = (oui, non)<br>
 *   - tresses = (oui, non)<br>
 *   - chignon = (oui, non)<br>
 *   - lunettes = (oui, non)<br>
 *   - couleurCheveux = (blond, blanc, gris, noir, brun, roux)<br>
 *   - longueurCheveux = (longs, moyens, courts, chauve)<br>
 *   - yeux = (bleu, marron, noir, gris, vert)<br>
 *   - couvreChef = (non, casquette, chapeau, béret, foulard)<br>
 * 
 * @author François Schumacker
 */
public class Suspect implements Comparable<Suspect>{

    // Variables d'instance
    private final String nom;
    private final String sexe;
    private final String moustache;
    private final String barbe;
    private final String tresses;
    private final String chignon;
    private final String lunettes;
    private final String couleurCheveux;
    private final String longueurCheveux;
    private final String yeux;
    private final String couvreChef;

    // Constructeur
    private Suspect(String nom, String sexe, String moustache, String barbe,
            String tresses, String chignon, String lunettes, String couleurCheveux,
            String longueurCheveux, String yeux, String couvreChef) {
        this.nom = nom;
        this.sexe = sexe;
        this.moustache = moustache;
        this.barbe = barbe;
        this.tresses = tresses;
        this.chignon = chignon;
        this.lunettes = lunettes;
        this.couleurCheveux = couleurCheveux;
        this.longueurCheveux = longueurCheveux;
        this.yeux = yeux;
        this.couvreChef = couvreChef;
    }

    // Interface Comparable
    @Override
    public int compareTo(Suspect other) {
        return this.nom.compareToIgnoreCase(other.nom);
    }
    
    // ------------------------------------------------------------------------
    // Méthodes et variables statiques
    // ------------------------------------------------------------------------

    // Ensemble de tous les suspects
    private final static TreeSet<Suspect> allSuspects = initSuspects();

    /**
     * Création de la base de données des suspects
     * @return 
     */
    private static TreeSet<Suspect> initSuspects() {
        TreeSet<Suspect> set = new TreeSet();
        set.add(new Suspect("Marie", "F", "non", "non", "non", "non", "non", "blond", "longs", "bleu", "chapeau"));
        set.add(new Suspect("Anne", "F", "non", "non", "oui", "oui", "non", "blanc", "moyens", "marron", "chapeau"));
        set.add(new Suspect("Claire", "F", "non", "non", "non", "non", "oui", "blonds", "longs", "noir", "non"));
        set.add(new Suspect("Isabelle", "F", "non", "non", "oui", "oui", "oui", "noir", "chauve", "gris", "foulard"));
        set.add(new Suspect("Sophie", "F", "non", "non", "non", "non", "non", "brun", "longs", "vert", "chapeau"));
        set.add(new Suspect("Martine", "F", "non", "non", "non", "oui", "non", "roux", "moyens", "bleu", "chapeau"));
        set.add(new Suspect("Séverine", "F", "non", "non", "non", "non", "oui", "blond", "courts", "marron", "non"));
        set.add(new Suspect("Pascale", "F", "non", "non", "oui", "oui", "oui", "blanc", "chauve", "noir", "foulard"));
        set.add(new Suspect("Emilie", "F", "non", "non", "non", "non", "non", "gris", "longs", "gris", "non"));
        set.add(new Suspect("Céline", "F", "non", "non", "non", "oui", "non", "noir", "moyens", "vert", "chapeau"));
        set.add(new Suspect("Julie", "F", "non", "non", "oui", "non", "oui", "brun", "courts", "bleu", "non"));
        set.add(new Suspect("Victoria", "F", "non", "non", "oui", "oui", "oui", "roux", "chauve", "marron", "foulard"));
        set.add(new Suspect("François", "M", "non", "non", "non", "non", "non", "blond", "longs", "noir", "casquette"));
        set.add(new Suspect("Maxime", "M", "non", "oui", "non", "non", "non", "blanc", "moyens", "bleu", "chapeau"));
        set.add(new Suspect("Richard", "M", "oui", "non", "non", "non", "oui", "gris", "courts", "vert", "béret"));
        set.add(new Suspect("Bernard", "M", "oui", "oui", "non", "non", "oui", "noir", "chauve", "gris", "non"));
        set.add(new Suspect("Robert", "M", "non", "non", "non", "non", "non", "brun", "longs", "marron", "casquette"));
        set.add(new Suspect("Paul", "M", "non", "oui", "non", "non", "non", "roux", "moyens", "noir", "chapeau"));
        set.add(new Suspect("Charles", "M", "oui", "non", "non", "non", "oui", "blond", "courts", "gris", "béret"));
        set.add(new Suspect("David", "M", "oui", "oui", "non", "non", "oui", "blanc", "chauve", "vert", "non"));
        set.add(new Suspect("Tom", "M", "non", "non", "non", "non", "oui", "gris", "longs", "bleu", "casquette"));
        set.add(new Suspect("Simon", "M", "non", "oui", "non", "non", "non", "noir", "moyens", "marron", "chapeau"));
        set.add(new Suspect("Georges", "M", "oui", "non", "non", "non", "oui", "brun", "courts", "noir", "béret"));
        set.add(new Suspect("Eric", "M", "oui", "oui", "non", "non", "oui", "roux", "chauve", "gris", "non"));
        return set;
    }

    /**
     * Effectue une recherche mono-critère dans la base de données des suspects<br>
     * et renvoie l'ensemble (éventuellement vide) des suspects sélectionnés.
     * 
     * @param property nom du critère de recherche ("sexe", "moustache", "barbe", ...)
     * @param value valeur recherchée pour ce critère ("M", "oui", "bleu", ...)
     * @return ensemble des suspects qui correspondent au critère recherché
     */
    public static TreeSet<Suspect> searchSuspectDatabase(String property, String value) {
        TreeSet<Suspect> result = new TreeSet<>();

        for (Suspect s : allSuspects) {
            boolean match = false;
            switch (property.toLowerCase()) {
                case "sexe":
                    match = s.sexe.equalsIgnoreCase(value);
                    break;
                case "moustache":
                    match = s.moustache.equalsIgnoreCase(value);
                    break;
                case "barbe":
                    match = s.barbe.equalsIgnoreCase(value);
                    break;
                case "tresses":
                    match = s.tresses.equalsIgnoreCase(value);
                    break;
                case "chignon":
                    match = s.chignon.equalsIgnoreCase(value);
                    break;
                case "lunettes":
                    match = s.lunettes.equalsIgnoreCase(value);
                    break;
                case "couleur_cheveux":
                case "couleurcheveux":
                    match = s.couleurCheveux.equalsIgnoreCase(value);
                    break;
                case "longueur_cheveux":
                case "longueurcheveux":
                    match = s.longueurCheveux.equalsIgnoreCase(value);
                    break;
                case "yeux":
                    match = s.yeux.equalsIgnoreCase(value);
                    break;
                case "couvre_chef":
                case "couvrechef":
                    match = s.couvreChef.equalsIgnoreCase(value);
                    break;
                default:
                    throw new IllegalArgumentException("Propriété inconnue : " + property);
            }
            if (match) {
                result.add(s);
            }
        }

        return result;
    }

    /**
     * Affiche la fiche détaillée d'un suspect.
     * @param s le suspect dont on souhaite afficher la fiche détaillée
     */
    public static void printSuspectDetails(Suspect s) {
        System.out.println("nom : " + s.nom);
        System.out.println("sexe : " + s.sexe);
        if (s.sexe.equals("M")) {
            System.out.println("moustache : " + s.moustache);
            System.out.println("barbe : " + s.barbe);
        } else {
            System.out.println("tresses : " + s.tresses);
            System.out.println("chignon : " + s.chignon);
        }
        System.out.println("lunettes : " + s.lunettes);
        System.out.println("longueurCheveux : " + s.longueurCheveux);
        System.out.println("couleurCheveux : " + s.couleurCheveux);
        System.out.println("yeux : " + s.yeux);
        System.out.println("couvre-chef : " + s.couvreChef);
    }

    /**
     * Affiche les noms des suspects présents dans l'ensemble de suspects fourni.
     * @param set ensemble de suspects
     */
    public static void printSuspectsNames(TreeSet<Suspect> set) {
        System.out.print("[");
        for(Suspect s : set) {
            System.out.print(" " + s.nom);
        }
        System.out.println(" ]");
    }
}
